
# Ciber Mall

> Sobre Ciber Mall

>La plataforma Ciber Mall, es la aplicación oficial para las tiendas convencionales que se encuentran físicamente en los Malls populares del país, pero en está ocasión la versión digital de las mismas.


## Requerimientos
- PHP 7.*
- MySQL 5.8.^ | MariaDB 11.^
- Laravel 7^
- Vue 2.*
- NPM 5.6.*
- Node 8.^

## Instalación

Una vez el proyecto ha sido clonado, realizar desde el directorio los siguientes pasos:

- git checkout origin develop
- composer install
- npm -g install
- Copiar el archivo 'example.env' a '.env'
- Crear base de datos en mysql y poblar los valores correspondientes en el archivo '.env'
- php artisan key:generate
- php artisan migrate:fresh --seed

## Servir el proyecto en entorno de desarrollo

- php artisan serve

## Credenciales de prueba

- webadmin@cibermall.com
- Webadmin2020

## Observaciones

Para liberar cache:

- php artisan config:clear
- php artisan route:list
- php artisan cache:clear
- composer dump-autoload

## Luego de actualización de código

Actualizar en el servidor:

- php artisan cache:clear
- php artisan config:cache

## Licencia

Todos los derechos reservados, Ciber Mall.
