<?php

namespace App\Models;

class ProductCategory extends BaseModel
{

    /**
     * The attributes that are guarded from mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

}
