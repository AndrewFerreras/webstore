<?php

namespace App\Models;

class Product extends BaseModel
{

    /**
     * The attributes that are guarded from mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product'];

}



