<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    /**
     * The attributes that are guarded from mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /* Queries Scopes */
    public function scopeActive($query)
    {
        $query->where('active', 1);
    }
}
