<?php

namespace App\Http\Controllers;

use App\Models\Warehouse;
use Illuminate\Http\Request;

class WarehousesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warehouses = Warehouse::active()->get();
        $model_text =  trans('models.warehouse') ;
        $model = 'warehouse';

        return view('manage.warehouses.index',compact('warehouses','model_text', 'model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.warehouses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $warehouse = new Warehouse($this->validateFields($request));

        $warehouse->save();

        $model = trans('models.warehouse');

        $request->session()->flash("flash_message",trans('messages.success_created',['model' =>  $model ]));

        return redirect('/manage/warehouses');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function show(Warehouse $warehouse)
    {
        return view('manage.warehouses.show', compact('warehouse'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function edit(Warehouse $warehouse)
    {
        return view('manage/warehouses/edit', compact('warehouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Warehouse $warehouse)
    {
        $warehouse->update($this->validateFields($request));

        $warehouse->save();

        $model = trans('models.warehouse');

        $request->session()->flash("flash_message",trans('messages.success_updated',['model' =>  $model ]));

        return redirect('/manage/warehouses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warehouse $warehouse)
    {
        $warehouse->active = 0;

        $warehouse->save();

        $model = trans('models.warehouse');

        request()->session()->flash("flash_message",trans('messages.success_deleted',['model' =>  $model ]));

        return redirect('/manage/warehouses');
    }

    public function validateFields(Request $request){

        $validatedData = $request->validate(
            [
                'name_code' => 'required',
                'description' => 'required',
                'location' => 'required'
            ],
            [
                'name_code.required' => trans('validation.filled', ['attribute' => 'código']),
                'description.required' => trans('validation.filled', ['attribute' => 'descripción']),
                'location.required' => trans('validation.filled', ['attribute' => 'ubicación'])
            ]
        );

        return $validatedData;
    }
}
