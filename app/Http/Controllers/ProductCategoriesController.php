<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productCategories = ProductCategory::all();
        $model_text =  trans('models.product_category') ;
        $model = 'productCategories';

        return view('manage.productCategories.index',compact('productCategories','model_text', 'model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.productCategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new ProductCategory($this->validateFields($request));

        $role->save();

        $model = trans('models.product_category');

        $request->session()->flash("flash_message",trans('messages.success_created',['model' =>  $model ]));

        return redirect('/manage/productCategories');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        return view('manage.productCategories.show', compact('productCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory)
    {
        return view('manage/productCategories/edit', compact('productCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $productCategory)
    {
        $productCategory->update($this->validateFields($request));

        $productCategory->save();

        $model = trans('models.product_category');

        $request->session()->flash("flash_message",trans('messages.success_updated',['model' =>  $model ]));

        return redirect('/manage/productCategories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $productCategory)
    {
        $productCategory->active = 0;

        $productCategory->save();

        $model = trans('models.product_category');

        request()->session()->flash("flash_message",trans('messages.success_deleted',['model' =>  $model ]));

        return redirect('/manage/productCategories');
    }

    public function validateFields(Request $request){

        $validatedData = $request->validate(
            [
                'name' => 'required',
                'description' => 'required',
                'slug' => 'required'
            ],
            [
                'name.required' => trans('validation.filled', ['attribute' => 'nombre']),
                'description.required' => trans('validation.filled', ['attribute' => 'descripción']),
                'slug.required' => trans('validation.filled', ['attribute' => 'frase corta'])
            ]
        );

        return $validatedData;
    }
}
