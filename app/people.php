<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class people extends Model
{
    public function scopeActive($query)
    {
        $query->where('active', 1);
    }
    protected $guarded = [];
    protected $fillable = [
        'name', 'email', 
    ];
}
