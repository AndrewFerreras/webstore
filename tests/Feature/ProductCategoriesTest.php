<?php

namespace Tests\Feature;

use App\Models\ProductCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class ProductCategoriesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function a_user_can_create_product_category()
    {
        $this->withExceptionHandling();

        $productCategory = new ProductCategory();
        $productCategory->name = $this->faker->name();
        $productCategory->description = $this->faker->sentence();
        $productCategory->slug = Str::of($productCategory->name)->slug('-');

        $this->get('/manage/productCategories/create')->assertStatus(200);

        $this->post('/manage/productCategories',$productCategory->toArray())->assertRedirect('/manage/productCategories');

        unset($productCategory->slug);

        $this->assertDatabaseHas('product_categories', $productCategory->toArray());
    }


    /**
     * @test
     */
    public function a_user_can_view_product_categories()
    {
        $productCategory = factory(ProductCategory::class)->create();
        $this->get('/manage/productCategories')->assertSee($productCategory->name);
    }

    /**
     * @test
     */
    public function a_user_can_view_a_product_category()
    {
        $productCategory = factory(ProductCategory::class)->create();
        $this->get('/manage/productCategories/'. $productCategory->id )->assertSee($productCategory->name);
    }

    /**
     * @test
     */
    public function a_user_can_update_a_product_category()
    {
        $productCategory = factory(ProductCategory::class)->create();
        $this->get('/manage/productCategories/'. $productCategory->id . '/edit')->assertSee($productCategory->name);

        $productCategory->name = $this->faker->name();
        $productCategory->description = $this->faker->sentence();
        $productCategory->slug = Str::of($productCategory->name)->slug('-');
        $productCategory->save();

        $this->get('/manage/productCategories/'. $productCategory->id . '/edit')->assertSee($productCategory->name);
    }

    /**
     * @test
     */
    public function a_user_can_delete_a_product_category()
    {
        $productCategory = factory(ProductCategory::class)->create();
        $this->get('/manage/productCategories/' . $productCategory->id  )->assertSee($productCategory->name);

        $productCategory->active = false;
        $productCategory->save();

        $this->get('/manage/productCategories/' . $productCategory->id  )->assertStatus(404);
    }

}
