<?php

namespace Tests\Feature;

use App\people;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PeopleTest extends TestCase
{
    /**
     * @test
     * */
    use RefreshDatabase , WithFaker;
       

            function a_people_can_registrer()
            {
                $this->withExceptionHandling();

            
                $peoples= new people();
                $peoples->name=$this->faker->name();
                $peoples->lastname=$this->faker->lastName();
                $peoples->email=$this->faker->email();
                $peoples->Address=$this->faker->address();
                $peoples->phone_number=$this->faker->phoneNumber();
                $peoples->date_of_birth=$this->faker->date('Y-m-d', 'now');
                $peoples->Bank_Card=$this->faker->creditCardNumber(null,true);

                $this->get('/manage/people/create')->assertStatus(200);

                $this->post('/manage/people',$peoples->toArray());
        
                unset($peopes);
        
               

            }
  /**
     * @test
     */
    public function a_user_can_view_people()
    {
        $people = factory(people::class)->create();
        $this->get('/manage/people')->assertSee($people->name);
        //las pruebas no aparecen 
    }
    

     /**
     * @test
     */
    public function a_user_can_update_a_people()
    {
        $peoples = factory(people::class)->create();
        $this->get('/manage/people/'. $peoples->id . '/edit')->assertSee($peoples->name);

        $peoples->name=$this->faker->name();
        $peoples->lastname=$this->faker->lastName();
        $peoples->email=$this->faker->email();
        
        $peoples->save();

        $this->get('/manage/people/'. $peoples->id . '/edit')->assertSee($peoples->name);
    }

    /**
     * @test
     */
    public function a_user_can_delete_a_people()
    {
        $peoples = factory(people::class)->create();
        $this->get('/manage/people/' . $peoples->id)->assertSee($peoples->name);

        $peoples->active = false;
        $peoples->save();

        
    }
}
 