<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function a_user_can_create_product()
    {
        $this->withoutExceptionHandling();

        $productCategory = factory(ProductCategory::class)->create();

        $product = new Product();
        $product->name = $this->faker->name();
        $product->description = $this->faker->sentence();
        $product->unit_price = $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1260);
        $product->units_in_stock = $this->faker->randomDigit;
        $product->product_category_id = $productCategory->id;

        $this->get('/manage/products/create')->assertStatus(200);

        $this->post('/manage/products', $product->toArray())->assertRedirect('/manage/products');

        $this->assertDatabaseHas('products', $product->toArray());
    }

    /**
     * @test
     */
    public function a_user_can_view_products()
    {
        factory(ProductCategory::class)->create();

        $product = factory(Product::class)->create();
        $this->get('/manage/products')->assertSee($product->name);
    }

    /**
     * @test
     */
    public function a_user_can_view_product()
    {
        factory(ProductCategory::class)->create();

        $product = factory(Product::class)->create();
        $this->get('/manage/products/'. $product->id )->assertSee($product->name);
    }

        /**
     * @test
     */
    public function a_user_can_update_a_product()
    {

        $productCategory = factory(ProductCategory::class)->create();

        $product = factory(Product::class)->create();
        $this->get('/manage/products/'. $product->id . '/edit')->assertSee($product->name);

        $product->name = $this->faker->name();
        $product->description = $this->faker->sentence();
        $product->unit_price = $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1260);
        $product->units_in_stock = $this->faker->randomDigit;
        $product->product_category_id = $productCategory->id;
        $product->save();

        $this->get('/manage/products/'. $product->id . '/edit')->assertSee($product->name);
    }

    /**
     * @test
     */
    public function a_user_can_delete_a_product()
    {
        $this->withoutExceptionHandling();
        
        factory(ProductCategory::class)->create();

        $product = factory(Product::class)->create();
        $this->get('/manage/products/' . $product->id  )->assertSee($product->name);

        $product->active = false;
        $product->save();
    }

}
