<?php

namespace Tests\Feature;

use App\Models\Warehouse;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class WarehousesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function a_user_can_create_warehouse()
    {
        $this->withExceptionHandling();

        $warehouse = new Warehouse();
        $warehouse->name_code = $this->faker->numerify('###');
        $warehouse->description = $this->faker->sentence();
        $warehouse->location = $this->faker->paragraph;

        $this->get('/manage/warehouses/create')->assertStatus(200);

        $this->post('/manage/warehouses',$warehouse->toArray())->assertRedirect('/manage/warehouses');

        $this->assertDatabaseHas('warehouses', $warehouse->toArray());
    }


    /**
     * @test
     */
    public function a_user_can_view_warehouses()
    {
        $warehouse = factory(Warehouse::class)->create();
        $this->get('/manage/warehouses')->assertSee($warehouse->name_code);
    }

    /**
     * @test
     */
    public function a_user_can_view_a_warehouse()
    {
        $warehouse = factory(Warehouse::class)->create();
        $this->get('/manage/warehouses/'. $warehouse->id )->assertSee($warehouse->name_code);
    }

    /**
     * @test
     */
    public function a_user_can_update_a_warehouse()
    {
        $warehouse = factory(Warehouse::class)->create();
        $this->get('/manage/warehouses/'. $warehouse->id . '/edit')->assertSee($warehouse->name_code);

        $warehouse->name_code = $this->faker->numerify('###');
        $warehouse->description = $this->faker->sentence();
        $warehouse->location = $this->faker->paragraph;
        $warehouse->save();

        $this->get('/manage/warehouses/'. $warehouse->id . '/edit')->assertSee($warehouse->name_code);
    }

    /**
     * @test
     */
    public function a_user_can_delete_a_warehouse()
    {
        $warehouse = factory(Warehouse::class)->create();
        $this->get('/manage/warehouses/' . $warehouse->id  )->assertSee($warehouse->name_code);

        $warehouse->active = false;
        $warehouse->save();

        $this->get('/manage/warehouses/' . $warehouse->id  )->assertStatus(200);
    }
}
