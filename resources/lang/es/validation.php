<?php

return [

    /*
    |--------------------------------------------------------------------------
    | válidation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the válidator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El campo :attribute debe ser aceptado.',
    'active_url'           => 'El campo :attribute es no es un URL válido.',
    'after'                => 'El campo :attribute debe ser una fecha despues de :date.',
    'alpha'                => 'El campo :attribute puede solamente contener letras.',
    'alpha_dash'           => 'El campo :attribute puede solamente contener letras, números, y guiones.',
    'alpha_num'            => 'El campo :attribute puede solamente contener letras y números.',
    'array'                => 'El campo :attribute debe ser un arreglo.',
    'before'               => 'El campo :attribute debe ser una fecha antes de :date.',
    'between'              => [
        'numeric' => 'El campo :attribute debe ser entre :min y :max.',
        'file'    => 'El campo :attribute debe ser entre :min y :max kilobytes.',
        'string'  => 'El campo :attribute debe ser entre :min y :max caracteres.',
        'array'   => 'El campo :attribute debe tener entre :min y :max articulos.',
    ],
    'boolean'              => 'El campo :attribute debe ser verdader o falso.',
    'confirmed'            => 'La confirmación del campo :attribute no es la correcta.',
    'date'                 => 'El campo :attribute no es una fecha válida.',
    'date_format'          => 'El campo :attribute no cumple con el formato :format.',
    'different'            => 'El campo :attribute y :other deben ser diferentes.',
    'digits'               => 'El campo :attribute debe ser de :digits diguitos.',
    'digits_entre'       => 'El campo :attribute debe ser entre :min y :max digits.',
    'email'                => 'El campo :attribute debe ser una dirección de correo válido.',
    'exists'               => 'El atributo seleccionado debe ser válido.',
    'filled'               => 'El campo :attribute es requerido.',
    'image'                => 'El campo :attribute debe ser una imagen.',
    'in'                   => 'El campo :attribute seleccionado es inválido.',
    'integer'              => 'El campo :attribute debe ser un numero entero.',
    'ip'                   => 'El campo :attribute debe ser una dirección IP válida.',
    'json'                 => 'El campo :attribute debe ser un  JSON string válido.',
    'max'                  => [
        'numeric' => 'El campo :attribute no puede ser mayor que :max.',
        'file'    => 'El campo :attribute no puede ser mayor que :max kilobytes.',
        'string'  => 'El campo :attribute no puede ser mayor que :max caracteres.',
        'array'   => 'El campo :attribute no puede tener mas de :max elmentos.',
    ],
    'mimes'                => 'El campo :attribute debe ser a file of type: :values.',
    'min'                  => [
        'numeric' => 'El campo :attribute debe ser al menos :min.',
        'file'    => 'El campo :attribute debe ser al menos :min kilobytes.',
        'string'  => 'El campo :attribute debe ser al menos :min caracteres.',
        'array'   => 'El campo :attribute debe have al menos :min elmentos.',
    ],
    'not_in'               => 'El campo :attribute  seleccionado es invalido.',
    'numeric'              => 'El campo :attribute debe ser un número.',
    'regex'                => 'El formato del campo :attribute es invalido.',
    'required'             => 'El  campo :attribute es requerido',
    'required_if'          => 'El campo :attribute field is required when :other is :value.',
    'required_with'        => 'El campo :attribute field is required when :values is present.',
    'required_with_all'    => 'El campo :attribute field is required when :values is present.',
    'required_without'     => 'El campo :attribute field is required when :values is not present.',
    'required_without_all' => 'El campo :attribute field is required when none of :values are present.',
    'same'                 => 'El campo :attribute y :other must match.',
    'size'                 => [
        'numeric' => 'El campo :attribute debe ser :size.',
        'file'    => 'El campo :attribute debe ser :size kilobytes.',
        'string'  => 'El campo :attribute debe ser :size caracteres.',
        'array'   => 'El campo :attribute debe contain :size elmentos.',
    ],
    'string'               => 'El campo :attribute debe ser un string.',
    'timezone'             => 'El campo :attribute debe ser una zona valida',
    'unique'               => 'El campo :attribute ya esta siendo utilizado.',
    'url'                  => 'El formato del campo :attribute  es invalido.',

    /*
    |--------------------------------------------------------------------------
    | Custom válidation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom válidation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom válidation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
