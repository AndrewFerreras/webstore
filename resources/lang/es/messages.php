<?php

return [
    'welcome' => 'Bienvenido!!',
    'app_name' => 'Virtual Mall',
    'app_short_name' => 'VirtualMall',

    /**
     *Pages name
     *
     */
    'recovery_password' => 'Recuperar de contraseña',
    'update_password' => 'Actualización de contraseña',
    'profile' => 'Perfil de Usuario',


    /**
    *Common
    *
    */
    'yes' => 'Si',
    'no' => 'No',
    'status' => 'Estado',
    'search' => 'Buscar',
    'percent' => 'Porcentaje',
    'no.'=>'No.',



    /**
    *Operations
    *
    */
    'actions' => 'Acciones',
    'create' => 'Crear',
    'view' => 'Ver',
    'edit' => 'Editar',
    'delete' => 'Eliminar',
    'save_changes' => 'Guardar cambios',
    'Cancel' => 'Cancelar',



    /**
    *Notificactions Messages
    *
    */
    'success_created' => 'El registro de :model ha sido exitosamente creado!!',
    'success_updated' => 'El registro de :model ha sido exitosamente actualizado!!',
    'success_deleted' => 'El registro de :model ha sido exitosamente eliminado!!',
    'success_flag' => 'Muy bien!',




    /**
    *Confirmatión Dialogs
    *
    */
    'dialog_confirmation' => 'Dialogo de Confirmación',
    'delete_confimation' => 'Usted esta apunto de eliminar un registro, este proceso es irreversible',
    'proceed_confirmation' => '¿Está seguro que desea proceder?',


    /**
    *Pagína login
    *
    */
    'invalid_credentials'=>'Credenciales no validas',


    /**
    *Pagína Perfil de Usuario
    *
    */
    'user_data'=>'Datos de usuario',
    'personal_data'=>'Datos personales',
    'comments'=>'Comentarios',
];
