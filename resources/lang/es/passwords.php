<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contarseña debe ser minimo 6 caracteres y ser la  misma que la confirmación',
    'reset' => 'Su contraseña ha sido reiniciada!!',
    'sent' => 'Le hemos enviado un correo con la información para reiniciar su contraseña',
    'token' => 'El token de reinicio de contraseña no es valido',
    'user' => "No existe ningun usuario con ese correo",
    'subject' => 'Su enlace para el reinicio de contraseña',
    'success_send_message' => 'Las instrucciones para reiniciar su contraseña, han sido enviadas a su correo electronico.',
    'update_password_success' => 'Su contraseña ha sido actualizada!',
];
