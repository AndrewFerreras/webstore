@extends('layouts.admin')

@section('main_content')
    <div class="container d-flex justify-content-between">
        <div>
            <h1>Productos</h1>
        </div>
        <div class="d-flex align-items-center">
            <a href="{{ action('Products' . 'Controller@create') }}" class="btn btn-block btn-primary">Crear Producto</a>
            
        </div>
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                @include('partials.flash')
                <div class="card-body">
                    <table id="product_categories_table" class="datatable table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Categoría</th>
                            <th>Precio Unitario</th>
                            <th>Unidades en Stock</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->description}} </td>
                                <td>{{ $product->categoryName}} </td>
                                <td>{{ $product->unit_price}} </td>
                                <td>{{ $product->units_in_stock}} </td>
                                <td>
                                    <div class="d-flex">
                                        <ul class="list-inline center mx-auto justify-content-center m-0">
                                            <li class="list-inline-item">
                                                <a class="nav-link" href="{{ url('/manage/products/' . $product->id ) }}"
                                                   productCategory="button"><i class="fas fa-book-open"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="nav-link"
                                                   href="{{ url('/manage/products/' . $product->id ) . '/edit' }}"
                                                   product="button"><i class="fas fa-edit"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="nav-link" href="#" product="button"
                                                   onclick="deleteModelRecord({{ $product->id }} , 'products') "><i
                                                        class="fas fa-trash-alt"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('partials.delete_dialog')
    <script>
    document.addEventListener("DOMContentLoaded", function () {

        $('.datatable').DataTable({
            "responsive": true,
            "autoWidth": false,
        });

    });
    </script>

@endsection
