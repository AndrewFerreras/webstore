@extends('layouts.admin')


@section('main_content')
    <div>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Producto</h1>
                    </div>
{{--                    <div class="col-sm-6">--}}
{{--                        <ol class="breadcrumb float-sm-right">--}}
{{--                            <li class="breadcrumb-item"><a href="#"></a></li>--}}
{{--                            <li class="breadcrumb-item active"></li>--}}
{{--                        </ol>--}}
{{--                    </div>--}}
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Detalles</h3>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">

                            <div class="row my-4">
                                <div class="col-sm-6">
                                    <h4>ID</h4>
                                    <label for="id">{{ $product->id  }}</label>
                                </div>
                                <div class="col-sm-6">
                                    <h4>Nombre</h4>
                                    <label for="name">{{ $product->name  }}</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <h4>Precio Unitario</h4>
                                    <label for="unit_price">{{ $product->unit_price  }}</label>
                                </div>
                                <div class="col-6">
                                    <h4>Unidades en Stock</h4>
                                    <label for="description">{{ $product->units_in_stock  }}</label>
                                </div>
                            </div>

                            <div class="row my-4" >
                                <div class="col-sm">
                                    <h4>Descripción</h4>
                                    <label for="description">{{ $product->description  }}</label>
                                </div>
                                
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



@endsection
