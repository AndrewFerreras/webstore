<div class="form-group">
    {!! Form::label('name', trans('models.name').': ') !!}
    {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Introducir nombre']) !!}
</div>

<div class="form-group">
    {!! Form::label('description',trans('models.description').': ') !!}
    {!! Form::text('description',null,['class'=>'form-control', 'placeholder'=>'Introducir descripción']) !!}
</div>


<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function() {
  $.validator.setDefaults({
    submitHandler: function () {
      $("#permission").submit();
    }
  });
  $('#permission').validate({
    rules: {
      name: {
        required: true
      },
      description: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Por favor introduzca el nombre del permiso."
      },
      description: {
        required: "Por favor introduzca la descripción del permiso."
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>