@extends('layouts.admin')


@section('main_content')
    <div>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Categoría producto</h1>
                    </div>
{{--                    <div class="col-sm-6">--}}
{{--                        <ol class="breadcrumb float-sm-right">--}}
{{--                            <li class="breadcrumb-item"><a href="#"></a></li>--}}
{{--                            <li class="breadcrumb-item active"></li>--}}
{{--                        </ol>--}}
{{--                    </div>--}}
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Detalles</h3>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">

                            <div class="row my-4">
                                <div class="col-sm-6">
                                    <h4>ID</h4>
                                    <label for="id">{{ $productCategory->id  }}</label>
                                </div>
                                <div class="col-sm-6">
                                    <h4>Nombre</h4>
                                    <label for="name">{{ $productCategory->name  }}</label>
                                </div>
                            </div>

                            <div class="row my-4" >
                                <div class="col-sm-6">
                                    <h4>Descripción</h4>
                                    <label for="description">{{ $productCategory->description  }}</label>
                                </div>
                                <div class="col-sm-6">
                                    <h4>Término</h4>
                                    <label for="slug">{{ $productCategory->slug  }}</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-body -->
                <a href="/manage/productCategories" class="btn btn-default float-right m-3">Regresar</a>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



@endsection
