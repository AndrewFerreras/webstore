<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{ asset('/images/CiberMallLogo200x200.png') }}" alt="Ciber Mall Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Ciber Mall Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('images/user-profile-pic.png') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">@if(isset( Auth::user()->name)) {{ Auth::user()->name }}@endif</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item ">
                    <a href="{{ url('/manage/dashboard') }}" class="nav-link @if(Request::is('*manage') || Request::is('*dashboard*')) active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tools"></i>
                        <p>
                            Negocio
                            <i class=" fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" style="display: block;">
                        <li class="nav-item">
                            <a href="{{ url('/manage/products') }}" class="nav-link  @if(Request::is('*products')) active @endif">
                                <i class="nav-icon fas fa-box-open"></i>
                                <p>
                                    Productos
                                </p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview" style="display: block;">
                        <li class="nav-item">
                            <a href="{{ url('/manage/productCategories') }}" class="nav-link  @if(Request::is('*productCategories')) active @endif">
                                <i class="nav-icon fas fa-barcode"></i>
                                <p>
                                    Categoría producto
                                </p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview" style="display: block;">
                        <li class="nav-item">
                            <a href="{{ url('/manage/warehouses') }}" class="nav-link  @if(Request::is('*warehouses')) active @endif">
                                <i class="nav-icon fas fa-warehouse"></i>
                                <p>
                                    Almacenes
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-unlock-alt"></i>
                        <p>
                            Seguridad
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" style="display: block;">
                        <li class="nav-item">
                            <a href="{{ url('/manage/users') }}" class="nav-link @if(Request::is('*users')) active @endif">
                                <i class="far fa-user-circle nav-icon"></i>
                                <p>Usuarios</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/manage/roles') }}" class="nav-link @if(Request::is('*roles')) active @endif">
                                <i class="fa fa-users nav-icon"></i>
                                <p>Roles</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/manage/permissions') }}" class="nav-link @if(Request::is('*permissions')) active @endif">
                                <i class="fa fa-check nav-icon"></i>
                                <p>Permisos</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/manage/people') }}"class="nav-link @if(Request::is('*people')) active @endif">
                                <i class="fa fa-users nav-icon"></i>
                                <p>Personas</p>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
