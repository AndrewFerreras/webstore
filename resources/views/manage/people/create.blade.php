@extends('layouts.admin')


@section('main_content')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->

        <div class="card card-primary mt-4 ">
            <div class="card-header mb-4">
                <h3 class="card-title">Crear persona</h3>
            </div>
            <div >
              @foreach ($errors->all() as $error)
              <div class="alert alert-danger alert-dismissible fade show" role="alert" >
                <strong>Error!</strong> {{ $error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endforeach
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" id="users" name="users" method="post" action="{{ url('/manage/people') }}">
                <div class="card-body">
                    @method('POST')
                    @csrf

                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="name" name="name" id="name" class="form-control"  placeholder="Introducir nombre"
                            >
                    </div>
                    <div class="form-group">
                        <label for="Lastname">Apellido</label>
                        <input type="name" name="Lastname" id="Lastname" class="form-control"  placeholder="Introducir Apellido"
                           >
                    </div>

                    <div class="form-group">
                        <label for="email">Correo electrónico</label>
                        <input type="email" name="email" id="email" class="form-control" 
                            placeholder="Introducir correo electrónico" >
                    </div>
                    <div class="form-group">
                        <label for="Address">Direccion</label>
                        <input type="text" name="Address" id="Address" class="form-control"  placeholder="Introducir Direccion"
                           >
                    </div>

                    <div class="form-group">
                        <label for="phone_number">Numero telefonico</label>
                        <input type="tel" name="phone_number" id="phone_number" class="form-control"  placeholder="Introducir Numero"
                           >
                    </div>
                    <div class="form-group">
                        <label for="date_of_birth">Fecha de nacimiento</label>
                        <input type="date" name="date_of_birth" id="date_of_birth" class="form-control"  placeholder="Introducir Fecha de nacimiento"
                           >
                    </div>

                    <div class="form-group">
                        <label for="Bank_Card">Tarjeta de credito</label>
                        <input type="string" name="Bank_Card" id="Bank_Card" class="form-control"  placeholder="Introducir Numero"
                           >
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right m-3">Crear</button>
                    <button type="reset" class="btn btn-default float-right m-3">Cancelar</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">
</div>
    <!--/.col (right) -->
    
    
    
    
@endsection