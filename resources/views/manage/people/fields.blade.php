
<div class="form-group">
    <label for="name">Nombre</label>
    <input type="name" name="name" id="name" class="form-control"  placeholder="Introducir nombre"
       value="{{$peoples->name}}" >
</div>
<div class="form-group">
    <label for="Lastname">Apellido</label>
    <input type="name" name="Lastname" id="Lastname" class="form-control"  placeholder="Introducir Apellido"
    value="{{$peoples->lastname}}"  >
</div>

<div class="form-group">
    <label for="email">Correo electrónico</label>
    <input type="email" name="email" id="email" class="form-control" 
        placeholder="Introducir correo electrónico"  value="{{$peoples->email}}">
</div>
<div class="form-group">
    <label for="Address">Direccion</label>
    <input type="text" name="Address" id="Address" class="form-control"  placeholder="Introducir Direccion"
       >
</div>

<div class="form-group">
    <label for="phone_number">Numero telefonico</label>
    <input type="tel" name="phone_number" id="phone_number" class="form-control"  placeholder="Introducir Numero"
       >
</div>
<div class="form-group">
    <label for="date_of_birth">Fecha de nacimiento</label>
    <input type="date" name="date_of_birth" id="date_of_birth" class="form-control"  placeholder="Introducir Fecha de nacimiento"
       >
</div>

<div class="form-group">
    <label for="Bank_Card">Tarjeta de credito</label>
    <input type="string" name="Bank_Card" id="Bank_Card" class="form-control"  placeholder="Introducir Numero"
       >
</div>
</div>