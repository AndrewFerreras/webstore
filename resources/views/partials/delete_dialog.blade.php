<pre delete-dialog-model="deleteModelRecord" class="d-none">

    <form id="deleteModelRecord" name="delteModelRecord" action="{{ url('/')}}" method="POST">
        @method('DELETE')
        @csrf
    </form>

    <script>
        
        function deleteModelRecord(id , model){
         
        url =  $('#deleteModelRecord').attr('action') + '/manage' + "/" +  model + "/" + id;

        Swal.fire({
            title: '¿Estás seguro que deseas eliminar este registro?',
            text: "La acción no podrá ser revertida!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminarlo!',
            cancelButtonText: 'Cancelar',
            }).then((result) => {
                if (result.value) {
                    
                    $('#deleteModelRecord').attr('action', url).submit();
                    
                }
            });
        }
        
    </script>
    </pre>