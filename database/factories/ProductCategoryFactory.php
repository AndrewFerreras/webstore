<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\ProductCategory;
use Faker\Generator as Faker;

$factory->define(ProductCategory::class, function (Faker $faker) {
    $jobTitle = $faker->jobTitle;
    return [
        "name" => $jobTitle,
        "description" => $faker->paragraph,
        "slug" => Str::of($jobTitle)->slug('-')
    ];
});
