<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Warehouse;
use Faker\Generator as Faker;

$factory->define(Warehouse::class, function (Faker $faker) {
    return [
        "name_code" => $faker->numerify('###'),
        "description" => $faker->sentence(),
        "location" => $faker->paragraph,
    ];
});
