<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Product;
use App\Models\ProductCategory;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $productsCategories = ProductCategory::pluck('id')->toArray();
    return [
        "name" => $faker->name,
        "description" => $faker->paragraph,
        "unit_price" => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1260),
        "units_in_stock" => $faker->randomDigit,
        "product_category_id" => $faker->randomElement($productsCategories)
    ];
});
