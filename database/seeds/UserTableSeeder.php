<?php

use Illuminate\Database\Seeder;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Administrador Web',
            'email' => 'webadmin@virtualmall.com',
            'password' => bcrypt('Webadmin2020'),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
       ]);

       $users = factory(App\User::class, 10)->create();
    }
}
