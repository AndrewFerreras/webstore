<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(ProductCategoryTableSeeder::class);
        $this->call(WarehousesTableSeeder::class);
        $this->call(PeopleTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
    }
}
